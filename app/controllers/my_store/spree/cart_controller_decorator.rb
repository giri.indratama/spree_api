module MyStore
  module Spree
    module CartControllerDecorator

      def create
        spree_authorize! :create, ::Spree::Order

        order_params = {
          user: spree_current_user,
          store: current_store,
          currency: current_currency
        }
        puts order_params
        puts 'Check Create'
        order   = spree_current_order if spree_current_order.present?
        order ||= create_service.call(order_params).value

        render_serialized_payload(201) { serialize_resource(order) }
      end

      def add_item
        spree_authorize! :update, spree_current_order, order_token
        spree_authorize! :show, @variant

        puts @variant

        result = add_item_service.call(
          order: spree_current_order,
          variant: @variant,
          quantity: params[:quantity],
          options: params[:options]
        )
        puts "CHECK Add ITEM"
        puts result

        render_order(result)
      end


      def show
        spree_authorize! :show, spree_current_order, order_token
        puts request.headers['Authorization']
        puts 'Check123'

        render_serialized_payload { serialized_current_order }
      end

      def add_item_service
        ::Spree::Api::Dependencies.storefront_cart_add_item_service.constantize
      end

      def load_variant
        @variant = ::Spree::Variant.find(params[:variant_id])
      end
    end
  end
end
Spree::Api::V2::Storefront::CartController.prepend MyStore::Spree::CartControllerDecorator